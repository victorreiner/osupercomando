### Missing in Action

*Missing in Action*, ou como conhecido no Brasil, *Braddock - O Super Comando* é um filme estadunidense de 1984, dos gêneros ação e guerra, dirigido por Joseph Zito, com roteiro de James Bruner, John Crowther, Lance Hool baseado em personagens criados por Arthur Silver, Larry Levinson e Steve Bing.

### A obra

A obra segue a onda dos tipos de filmes que eram lançados na época, como *Rambo*, em que consistiam em apresentar heróis bem casca-grossa formando "exércitos de um homem só" e assim demonstrar todo o poder americano em relação aos inimigos russos, vietnamitas e etc. 
Porém, este filme em específico conta com um grande aspecto que o fez especial no coração dos amantes do gênero, o ator principal, Chuck Norris, o qual realizava tarefas impossíveis como carregar armas pesadíssimas com uma só mão, ou vencer um grande número de inimigos sozinho, e sempre interpretando todas as cenas de forma igual, reagindo as cenas de ação e de drama da mesma forma.

### Sinopse

O coronel das Forças Especiais James Braddock precisa trazer de volta soldados norte-americanos que ainda estão sendo mantidos como prisioneiros no Vietnã. Contando com a ajuda da funcionária do Departamento de Estado e de um ex-colega de Exército, Braddock reúne informação altamente confidencial e armamento de última geração. Braddock forma então um exército de um homem só, disposto a invadir o Vietnã para localizar e salvar seus companheiros restantes desaparecidos em combate.

### Elenco

- Chuck Norris - James Braddock
- Lenore Kasdorf - Ann
- M. Emmet Walsh - Tuck
- David Tress - senador Porter
- James Hong - general Trau
- Ernie Ortega - Vinh
- Pierrino Mascarino - Jacques
- Erich Anderson - Masucci

Retirado e adaptado de https://pt.wikipedia.org/wiki/Missing_in_Action