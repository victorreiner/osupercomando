---
title: "Chuck Norris"
date: 2021-10-20T11:38:57-03:00
draft: false
---
Carlos Ray "Chuck" Norris (Ryan, 10 de março de 1940) é um artista de artes marciais, ator, produtor de cinema, argumentista americano. Depois de servir na Força Aérea dos Estados Unidos, começou a tornar-se conhecido como praticante de artes marciais. Em 1990, fundou a sua escola de artes marciais, Chun Kuk Do, e em 2005, a World Combat League (WCL), uma competição de combate por equipes.

Norris apareceu em numerosos filmes de acção, como Way of the Dragon (1972), em que contracena com Bruce Lee, Lone Wolf McQuade (1983) com David Carradine, na trilogia Missing in Action e em The Delta Force (1986) com Lee Marvin. Foi a estrela maior da empresa The Cannon Group, e o actor principal da série de televisão Walker, Texas Ranger, exibida de 1993 a 2001.

Retirado de Wikipedia