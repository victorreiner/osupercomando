---
title: "Verdades verdadeiras sobre Chuck Norris"
date: 2021-10-20T11:39:12-03:00
draft: false
---

1. Chuck Norris contou até o infinito. Duas vezes.

2. Chuck Norris pediu um Big Mac no Bob’s. Ele foi atendido.

3. Os dinossauros olharam torto para Chuck Norris uma vez. Uma vez.

4. Quando o Bicho Papão vai dormir, ele deixa a luz acesa com medo de Chuck Norris.

5. Antes de esquecer um presente de Chuck Norris, Papai Noel existia.

6. Se "O Exterminador do Futuro" fosse com Chuck Norris, ele seria um documentário.

7. Chuck Norris sabe qual é o último algarismo do pi.

8. Uma vez Chuck Norris foi picado por uma cobra venenosa, e após 2 dias de dor agonizante a cobra morreu.

9. Chuck Norris e Super-Homem uma vez apostaram uma queda de braço, o perdedor teria que usar a cueca por cima da calça.

10. Uma vez Chuck Norris jogou uma granada e matou 500 pessoas de uma vez. Logo depois a granada explodiu.

11. Chuck Norris zerou Metal Slug sem morrer.

12. O título original para Star Wars era "Skywalker: Texas Ranger", estrelando Chuck Norris.