---
title: "Curiosidades"
date: 2021-10-20T12:14:26-03:00
draft: false
---
### Filme em homenagem ao irmão

Chuck Norris disse que o filme foi feito como uma forma de homenagem ao seu irmão mais novo, morto na Guerra do Vietnã em 1970.

### Van Damme

Jean Claude Van Damme trabalhou como dublê no filme.

### Braddock 2 

Um ano após o primeiro filme foi lançado *Braddock 2 – O Início da Missão*, que inicialmente foi gravado como o primeiro filme da trilogia, porém, por acharem *O Super Comando* melhor, inverteram a ordem de lançamento.

### Braddock 3

*Braddock 3 – O Resgate* foi lançado em 1988, e durante suas filmagens um helicóptero caiu e matou 5 pessoas.

Fonte: http://www.cinemaepipoca.com.br/braddock-a-franquia/